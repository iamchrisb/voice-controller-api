package de.bht.interactive.vc.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class VCApplication extends Application {

}
