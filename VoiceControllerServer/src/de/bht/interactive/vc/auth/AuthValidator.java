package de.bht.interactive.vc.auth;

import java.security.NoSuchAlgorithmException;

import de.bht.voic.shared.model.rest.User;
import de.bht.voic.shared.model.rest.UserInformation;

public class AuthValidator {

	private static final Integer MIN_CIPHERS = 8;
	// private static final String PASSWORD_REGEX =
	// "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{"
	// + MIN_CIPHERS + ",}";
	private static final String PASSWORD_REGEX = ".*";

	public static void validateUser(User user) throws IllegalArgumentException {
	}

	public static void validateUserInformation(UserInformation userInformation) throws IllegalArgumentException {
		isUndefined(userInformation.getEmail());
	}

	private static boolean isUndefined(Object obj) throws IllegalArgumentException {
		if (obj instanceof Number) {
			return obj == null;
		} else if (obj instanceof String) {
			return obj == null || ((String) obj).isEmpty();
		}
		throw new IllegalArgumentException("Missing required attributes.");
	}
	
	public static boolean validatePassword(String password) {
		return password == null || password.matches(PASSWORD_REGEX);
		// if (password == null || password.matches(passwordRegex)) {
		// throw new SecurityException("password must be at least" + MIN_CIPHERS
		// +
		// "characters long contain a number, no whitespaces, special character and an uppercase letter.");
		// }
	}

	public static boolean validatePassword(final String plainPassword, final String cipherPassword, final String storedSalt) throws SecurityException {
		String encodedPassword = "";
		try {
			encodedPassword = PasswordEncoder.encodePBKDF2WithHmacSHA1(plainPassword, storedSalt);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encodedPassword.equals(cipherPassword);	
	}
}
