package de.bht.interactive.vc.auth;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordEncoder {

	private final static Integer ITERATIONS = 1000;
	private final static Integer KEY_LENGTH = 64 * 8;

	public static String generateSHA1PRNGSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = null;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (sr == null) {
			return null;
		}
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return toHex(salt);
	}

	public static String encodePBKDF2WithHmacSHA1(String password, String salt) throws NoSuchAlgorithmException {
		char[] pwdCharacterArray = password.toCharArray();
		PBEKeySpec spec = new PBEKeySpec(pwdCharacterArray, fromHex(salt), ITERATIONS, KEY_LENGTH);
		Arrays.fill(pwdCharacterArray, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			return toHex(skf.generateSecret(spec).getEncoded());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} finally {
			spec.clearPassword();
		}
	}
	
	private static String toHex(byte[] array) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}
}
