package de.bht.interactive.vc.auth;

import java.util.HashMap;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class SessionStorage {

	private HashMap<String, String> sessionTokens = new HashMap<String, String>();

	@Lock(LockType.WRITE)
	public void put(String sessionToken, String user) {
		sessionTokens.put(sessionToken, user);
	}

	@Lock(LockType.WRITE)
	public String remove(String sessionToken) {
		return sessionTokens.remove(sessionToken);
	}

	@Lock(LockType.READ)
	public String get(String sessionToken) {
		return sessionTokens.get(sessionToken);
	}

	@Lock(LockType.READ)
	public boolean contains(String sessionToken) {
		return sessionTokens.containsKey(sessionToken);
	}

}
