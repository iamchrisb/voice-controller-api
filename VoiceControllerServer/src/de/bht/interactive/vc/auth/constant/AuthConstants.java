package de.bht.interactive.vc.auth.constant;

public class AuthConstants {

	public static final String API_KEY = "123456";
	public static final String HEADER_API_KEY = "api_key";
	public static final String HEADER_SESSION_TOKEN = "session_token";
	public static final String HEADER_BASIC_AUTH_TOKEN = "Authorization";
	public static final String HEADER_AUTH_SPLITTER = "&";
}
