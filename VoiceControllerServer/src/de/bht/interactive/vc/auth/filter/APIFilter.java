package de.bht.interactive.vc.auth.filter;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.core.ResourceMethodInvoker;

import de.bht.interactive.vc.auth.constant.AuthConstants;
import de.bht.interactive.vc.auth.model.AuthResponse;

@Provider
public class APIFilter implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext
				.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
		Method method = methodInvoker.getMethod();

		if (!method.isAnnotationPresent(PermitAll.class)) {
			
			if(method.isAnnotationPresent(DenyAll.class)) {
				Response response = Response.status(Status.UNAUTHORIZED)
						.entity(new AuthResponse(null, "Nobody can access this resource!")).build();
				requestContext.abortWith(response);
			}
			
			String serviceKey = requestContext.getHeaderString(AuthConstants.HEADER_API_KEY);
			if (serviceKey == null || !serviceKey.equals(AuthConstants.API_KEY)) {
				Response response = Response.status(Status.UNAUTHORIZED)
						.entity(new AuthResponse(null, "Invalid api key")).build();
				requestContext.abortWith(response);
			}
		}
	}
}
