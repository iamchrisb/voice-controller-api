package de.bht.interactive.vc.auth.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import de.bht.voic.shared.model.rest.User;
import de.bht.voic.shared.model.rest.UserInformation;

@JsonInclude(Include.NON_NULL)
public class AuthResponse {

	private String sessionToken;
	private String error;
	private User user;
	private UserInformation userInformation;

	public AuthResponse(String sessionToken, String error, User user, final UserInformation userInformation) {
		this.sessionToken = sessionToken;
		this.error = error;
		this.user = user;
		this.userInformation = userInformation;
	}

	public AuthResponse(String sessionToken, String error) {
		this.sessionToken = sessionToken;
		this.error = error;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserInformation getUserInformation() {
		return userInformation;
	}

	public void setUserInformation(UserInformation userInformation) {
		this.userInformation = userInformation;
	}
}
