package de.bht.interactive.vc.auth.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.util.Base64;

import de.bht.interactive.vc.auth.AuthValidator;
import de.bht.interactive.vc.auth.PasswordEncoder;
import de.bht.interactive.vc.auth.SessionStorage;
import de.bht.interactive.vc.auth.annotation.NoSession;
import de.bht.interactive.vc.auth.constant.AuthConstants;
import de.bht.interactive.vc.auth.model.AuthResponse;
import de.bht.interactive.vc.dao.UserDAO;
import de.bht.interactive.vc.dao.UserInformationDAO;
import de.bht.voic.shared.model.rest.User;
import de.bht.voic.shared.model.rest.UserInformation;

@Stateless
@Path("/auth")
public class AuthService {

	@EJB
	SessionStorage sessionStorage;

	@EJB
	private UserDAO userDAO;

	@EJB
	private UserInformationDAO userInformationDAO;

	@POST
	@NoSession
	@Path("/register/form")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerForm(@FormParam("username") String username, @FormParam("password") String password,
			@FormParam("email") String email) {

		// use validator
		if (password == null || password.length() == 0) {
			return Response.status(Status.FORBIDDEN).entity("you have to submit a password").build();
		}

		if (email == null || email.length() == 0) {
			return Response.status(Status.FORBIDDEN).entity("you have to submit an email").build();
		}

		if (username == null || username.length() == 0) {
			return Response.status(Status.FORBIDDEN).entity("you have to submit a username").build();
		}

		try {
			// AuthValidator.validateCustomer(customer);
		} catch (IllegalArgumentException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}

		try {
			// AuthValidator.validatePassword(decodedPassword);
		} catch (SecurityException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}

		String salt;
		String encodedSHAPassword;
		try {
			salt = PasswordEncoder.generateSHA1PRNGSalt();
			encodedSHAPassword = PasswordEncoder.encodePBKDF2WithHmacSHA1(password, salt);
		} catch (NoSuchAlgorithmException e) {
			return Response.status(Status.FORBIDDEN).entity("Unable to create credentials for this user").build();
		}

		User user = new User(username, encodedSHAPassword, salt);
		user = userDAO.create(user);

		if (user == null) {
			return Response.status(Status.BAD_REQUEST).entity(new AuthResponse(null, "User not registered")).build();
		}

		UserInformation userInformation = new UserInformation();
		userInformation.setId(user.getId());
		userInformationDAO.create(userInformation);

		return Response.ok().entity(user).build();
	}

	@POST
	@NoSession
	@Path("register/basic")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerBasic(@Context HttpHeaders httpHeaders) {
		String headerString = httpHeaders.getHeaderString(AuthConstants.HEADER_BASIC_AUTH_TOKEN);
		String encodedHeaderString = headerString.split(" ")[1];
		String decodedHeaderString = "";

		try {
			decodedHeaderString = new String(Base64.decode(encodedHeaderString));
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("invalid Base64 format").build();
		}

		String[] userData = decodedHeaderString.split(AuthConstants.HEADER_AUTH_SPLITTER);

		String submittedUsername = userData[0];
		String submittedEmail = userData[1];
		String submittedPassword = userData[2];

		return registerForm(submittedUsername, submittedPassword, submittedEmail);
	}

	@POST
	@Path("/login/form")
	@NoSession
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginForm(@FormParam("username") String username, @FormParam("password") String password) {

		User user = userDAO.findByUsername(username);
		if (user == null) {
			return Response.status(Status.UNAUTHORIZED).entity(new AuthResponse(null, "User not found")).build();
		}

		if (!AuthValidator.validatePassword(password, user.getPassword(), user.getSalt())) {
			return Response.status(Status.UNAUTHORIZED).entity("wrong password").build();
		}

		String sessionToken = UUID.randomUUID().toString();
		sessionStorage.put(sessionToken, username);

		UserInformation userInformation = userInformationDAO.find(user.getId());

		return Response.ok().entity(new AuthResponse(sessionToken, null, user, userInformation)).build();
	}
	
	@POST
	@Path("/login/basic")
	@NoSession
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginBasic(@Context HttpHeaders httpHeaders) {
		String encodedCredentials = httpHeaders.getHeaderString(AuthConstants.HEADER_BASIC_AUTH_TOKEN).split(" ")[1];
		String decodedCredentials = "";

		try {
			decodedCredentials = new String(Base64.decode(encodedCredentials));
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("invalid Base64 format").build();
		}

		if (decodedCredentials == null || decodedCredentials.length() == 0) {
			return Response.status(Status.FORBIDDEN).entity("no user name and password").build();
		}

		String[] splittedCredentials = decodedCredentials.split("&");
		String username = splittedCredentials[0];
		String password = splittedCredentials[1];

		return loginForm(username, password);
	}

	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout(@Context HttpHeaders httpHeaders) {
		String sessionToken = httpHeaders.getHeaderString(AuthConstants.HEADER_SESSION_TOKEN);
		sessionStorage.remove(sessionToken);
		return Response.ok().entity(new AuthResponse(sessionToken, null)).build();
	}

	@POST
	@Path("/change")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response change(@Context HttpHeaders httpHeaders, @FormParam("password") String password) {

		String sessionToken = httpHeaders.getHeaderString(AuthConstants.HEADER_SESSION_TOKEN);
		String username = sessionStorage.get(sessionToken);

		User user = userDAO.findByUsername(username);
		if (user == null) {
			Response.status(Status.UNAUTHORIZED).entity(new AuthResponse(null, "User not found")).build();
		}
		user.setPassword(password);
		userDAO.update(user);

		return Response.ok().entity(user).build();
	}
}
