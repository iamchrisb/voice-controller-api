package de.bht.interactive.vc.dao;

import java.math.BigInteger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.bht.voic.shared.model.rest.Command;

@Stateless
@LocalBean
public class CommandDAO extends GenericJpaDAO<Command, BigInteger> {

	@PersistenceContext(unitName = "vc")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Class<Command> getPersistenceClass() {
		return Command.class;
	}

}
