package de.bht.interactive.vc.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public interface GenericDAO<E extends Serializable, ID extends Serializable> {

	public E create(E entity);
	public void remove(ID id);
	public E update(E entity);
	public E find(ID id);
	public void flush();
	public List<E> query(String queryString, HashMap<String, Object> attributes);
	public List<E> query(String queryString);
	public List<E> query(HashMap<String, Object> attributes);
	public List<E> query(String fieldname, Long beginTimeMillis, Long endTimeMillis,HashMap<String, Object> attributes);
	public List<E> query();
	public <T> List<T> query(String queryString, HashMap<String, Object> attributes, Class<T> clazz);
	public <T> List<T> query(String queryString, Class<T> clazz);
}
