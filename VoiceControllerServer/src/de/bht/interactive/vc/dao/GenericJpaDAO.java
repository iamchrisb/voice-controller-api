package de.bht.interactive.vc.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jboss.logging.Logger;

public abstract class GenericJpaDAO<E extends Serializable, ID extends Serializable>
		implements GenericDAO<E, ID> {

	@Override
	public E create(E entity) {
		getEntityManager().persist(entity);
		getEntityManager().flush();
		return entity;
	}

	@Override
	public void remove(ID id) {
		E entity = find(id);
		if (entity != null) {
			getEntityManager().remove(entity);
		}
	}

	@Override
	public E update(E entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public E find(ID id) {
		return getEntityManager().find(getPersistenceClass(), id);
	}
	
	@Override
	public void flush() {
		getEntityManager().flush();
	}

	@Override
	public List<E> query(String queryString, HashMap<String, Object> attributes) {
		TypedQuery<E> query = getEntityManager().createQuery(queryString,
				getPersistenceClass());

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		List<E> result = query.getResultList();

		if (result.isEmpty()) {
			Logger.getLogger(this.getClass().getName()).log(Logger.Level.DEBUG,
					getTable() + ": Empty Result > " + queryString);
		}

		return result;
	}

	@Override
	public List<E> query(String queryString) {
		return query(queryString, new HashMap<String, Object>());
	}
	
	@Override
	public List<E> query(HashMap<String, Object> attributes) {
		

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<E> q = cb.createQuery(this.getPersistenceClass());
		Root<E> root = q.from(this.getPersistenceClass());
		
		StringBuilder log = new StringBuilder("filter: ");

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		for (Map.Entry<String, Object> attribute : attributes.entrySet()) {
			if(attribute.getValue() != null) {
				predicates.add(cb.equal(root.get(attribute.getKey()), attribute.getValue()));
				log.append(attribute.getKey() + ": " + attribute.getValue());
			}
		}

		q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		q.select(root);

		TypedQuery<E> query = getEntityManager().createQuery(q);
		List<E> results = query.getResultList();
		
		if(results.isEmpty()) {
			Logger.getLogger(this.getClass().getName()).log(Logger.Level.DEBUG,
					getTable() + ": Empty Result > " + log);
		}
		
		return results;
	}
	
	@Override
	public List<E> query() {
		return this.query(new HashMap<String, Object>());
	}

	@Override
	public <T> List<T> query(String queryString,
			HashMap<String, Object> attributes, Class<T> clazz) {

		TypedQuery<T> query = getEntityManager()
				.createQuery(queryString, clazz);

		for (Map.Entry<String, Object> attribute : attributes.entrySet()) {
			query.setParameter(attribute.getKey(), attribute.getValue());
		}

		List<T> result = query.getResultList();

		if (result.isEmpty()) {
			Logger.getLogger(this.getClass().getName()).log(Logger.Level.DEBUG,
					getTable() + ": Empty Result > " + queryString);
		}
		return result;
	}
	
	@Override
	public List<E> query(String fieldname, Long min, Long max, HashMap<String, Object> attributes) {
		
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<E> q = cb.createQuery(this.getPersistenceClass());
		Root<E> root = q.from(this.getPersistenceClass());
		 
		StringBuilder log = new StringBuilder("filter: ");

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (!(min == 0 && max == 0)) {
			try {
				log.append(fieldname + ": " + "[min: " + min + " max: " + max + "] ");
				predicates.add(cb.between(root.<Long>get(fieldname), min, max));
			} catch(IllegalArgumentException ex) {
				Logger.getLogger(this.getClass().getName()).log(Logger.Level.DEBUG,
						getTable() + ": Empty Result > " + fieldname + " is not a valid range field!" );
				return new ArrayList<E>();
			}
		}

		for (Map.Entry<String, Object> attribute : attributes.entrySet()) {
			if(attribute.getValue() != null) {
				predicates.add(cb.equal(root.get(attribute.getKey()), attribute.getValue()));
				log.append(attribute.getKey() + ": " + attribute.getValue());
			}
		}
		
		q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		q.select(root);

		TypedQuery<E> query = getEntityManager().createQuery(q);
		List<E> result = query.getResultList();
		
		if(result.isEmpty()) {
			Logger.getLogger(this.getClass().getName()).log(Logger.Level.DEBUG,
					getTable() + ": Empty Result > " + log);
		}
		
		return result;
	}
	
	@Override
	public <T> List<T> query(String queryString, Class<T> clazz) {
		return query(queryString, new HashMap<String, Object>(), clazz);
	}

	public String getTable() {
		return getPersistenceClass().getSimpleName();
	}

	public abstract EntityManager getEntityManager();

	public abstract Class<E> getPersistenceClass();

}




