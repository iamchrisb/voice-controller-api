package de.bht.interactive.vc.dao;

import java.math.BigInteger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.bht.voic.shared.model.rest.Profile;

@Stateless
@LocalBean
public class ProfileDAO extends GenericJpaDAO<Profile, BigInteger> {

	@PersistenceContext(unitName = "vc")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Class<Profile> getPersistenceClass() {
		return Profile.class;
	}
}
