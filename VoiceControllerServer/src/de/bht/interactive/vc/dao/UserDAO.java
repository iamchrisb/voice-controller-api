package de.bht.interactive.vc.dao;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.bht.voic.shared.model.rest.User;

@LocalBean
@Stateless
public class UserDAO extends GenericJpaDAO<User, BigInteger> {

	@PersistenceContext(unitName = "vc")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Class<User> getPersistenceClass() {
		return User.class;
	}

	public User findByUsername(String username) {
		if (username == null) {
			return null;
		}

		HashMap<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("name", username);
		List<User> result = query(attributes);
		if (!result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}
}
