package de.bht.interactive.vc.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.bht.interactive.vc.auth.annotation.NoSession;
import de.bht.interactive.vc.dao.CommandDAO;
import de.bht.voic.shared.model.rest.Command;
import de.bht.voic.shared.model.rest.KeyAction;

@Stateless
@Path("/commands")
public class CommandService {

	@EJB
	private CommandDAO commandDAO;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommand(@PathParam("id") BigInteger id) {
		Command command = commandDAO.find(id);
		return Response.ok(command).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@NoSession
	public Response getCommands(@QueryParam("word") String word, @QueryParam("profileId") BigInteger profileId) {
		HashMap<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("word", word);
		attributes.put("profileId", profileId);
		List<Command> commands = commandDAO.query(attributes);
		return Response.ok().entity(commands).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCommands(Command[] commands) {
		if (commands == null) {
			return Response.ok().entity("emtpy command list").build();
		}

		List<Command> storedCommands = new ArrayList<Command>();

		for (Command currentCommand : commands) {
			currentCommand = commandDAO.create(currentCommand);
			storedCommands.add(currentCommand);
		}

		return Response.ok(storedCommands).build();
	}

	@POST
	@Path("/{id}")
	@NoSession
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCommand(@PathParam("id") BigInteger id, Command command) {
		command.setId(id);
		command = commandDAO.update(command);
		return Response.ok(command).build();
	}

	@DELETE
	@Path("/{id}")
	@NoSession
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCommand(@PathParam("id") BigInteger id) {
		commandDAO.remove(id);
		return Response.ok().build();
	}

	@GET
	@Path("/test")
	@NoSession
	@Produces(MediaType.APPLICATION_JSON)
	public Response test() {
		Command cmd = new Command();
		cmd.setWord("walk");

		KeyAction action = new KeyAction();
		ArrayList<Integer> keycodes = new ArrayList<Integer>();
		keycodes.add(86);
		keycodes.add(87);
		action.setKeycodes(keycodes);

		ArrayList<Integer> delays = new ArrayList<Integer>();
		delays.add(300);
		delays.add(600);
		action.setDelays(delays);
		cmd.setAction(action);
		cmd = commandDAO.create(cmd);

		return Response.ok().build();
	}
}
