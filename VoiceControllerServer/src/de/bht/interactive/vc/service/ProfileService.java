package de.bht.interactive.vc.service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.bht.interactive.vc.dao.ProfileDAO;
import de.bht.voic.shared.model.rest.Profile;

@Stateless
@Path("/profiles")
public class ProfileService {

	@EJB
	private ProfileDAO profileDAO;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProfile(@PathParam("id") BigInteger id) {
		Profile profile = profileDAO.find(id);
		return Response.ok(profile).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProfiles(@QueryParam("created") Long created, 
			@QueryParam("modified") Long modified, @QueryParam("isPublic") Boolean isPublic, 
			@QueryParam("userId") BigInteger userId) {
		
		HashMap<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("created", created);
		attributes.put("modified", modified);
		attributes.put("isPublic", isPublic);
		attributes.put("userId", userId);
		
		List<Profile> profiles = profileDAO.query(attributes);
		
		return Response.ok(profiles).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createProfile(Profile profile) {
		profile = profileDAO.create(profile);
		return Response.ok(profile).build();
	}

	@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProfile(@PathParam("id") BigInteger id, Profile profile) {
		profile.setId(id);
		profile  = profileDAO.update(profile);
		return Response.ok(profile).build();
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProfile(@PathParam("id") BigInteger id) {
		profileDAO.remove(id);
		return Response.ok().build();
	}

}
