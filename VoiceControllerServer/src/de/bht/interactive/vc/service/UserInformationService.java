package de.bht.interactive.vc.service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.bht.interactive.vc.dao.UserInformationDAO;
import de.bht.voic.shared.model.rest.UserInformation;

@Stateless
@Path("/userinfos")
public class UserInformationService {
	
	@EJB
	private UserInformationDAO userInformationDAO;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInformation(@PathParam("id") BigInteger id) {
		UserInformation userInformation = userInformationDAO.find(id);
		return Response.ok(userInformation).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInformations() {
		HashMap<String, Object> attributes = new HashMap<String, Object>();
		List<UserInformation> userInformations = userInformationDAO.query(attributes);
		return Response.ok(userInformations).build();
	}
	
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUserInformation(@PathParam("id") BigInteger id, UserInformation userInformation) {
		userInformation.setId(id);
		userInformation = userInformationDAO.update(userInformation);
		return Response.ok(userInformation).build();
	}
}
