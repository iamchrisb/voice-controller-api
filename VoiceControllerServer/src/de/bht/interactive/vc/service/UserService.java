package de.bht.interactive.vc.service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.bht.interactive.vc.auth.SessionStorage;
import de.bht.interactive.vc.dao.UserDAO;
import de.bht.voic.shared.model.rest.User;

@Stateless
@Path("/users")
public class UserService {

	@EJB
	private UserDAO userDAO;

	@EJB
	private SessionStorage sessionStorage;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("id") BigInteger id) {
		User user = userDAO.find(id);
		return Response.ok(user).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers() {

		HashMap<String, Object> attributes = new HashMap<String, Object>();
		List<User> users = userDAO.query(attributes);

		return Response.ok(users).build();
	}
}
